const Users = require("../models/Users");
const Products = require("../models/Products");


// Product registration
module.exports.productRegistration = (requestBody, usersData) => {

	//console.log(usersData.userId);

	return Users.findById(usersData.userId).then(result => {

		if (usersData.isAdmin == false) {
			//return false
			return "Only an admin can register new product.";
		} else {

			let newProducts = new Products({
				name: requestBody.name,
				description: requestBody.description,
				price: requestBody.price
			});

			return newProducts.save().then((product, error) => {

				if (error) {
					console.log(error);
					return "Product registration failed.";
				} else {
					return "Product registration successful!";
				}

			});
		}
	});
}


module.exports.activeProductRetrieval = () => {

	return Products.find({isActive: true}).then(result => {
		return result;
	});
}