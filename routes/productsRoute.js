const express = require("express");
const router = express.Router();
const productsController = require("../controllers/productsController");
const authentication = require("../auth");


// Route for creating a product
router.post("/", authentication.tokenVerification, (request, response) => {

	const usersData = authentication.tokenDecryption(request.headers.authorization);

	//console.log(usersData);

	productsController.productRegistration(request.body, {userId: usersData.id, isAdmin: usersData.isAdmin}).then(controllerResult => response.send(controllerResult));
});


router.get("/", (request, response) => {

	productsController.activeProductRetrieval().then(controllerResult => response.send(controllerResult));

});


module.exports = router;