const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "Name of product is required."]
	},
	description: {
		type: String
	},
	price: {
		type: Number,
		required: [true, "Price is required."]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	orders: [
		{
			orderId: {
				type: String,
				required: [true, "Order ID is required for inventory purposes."]
			}
		}
	]
});

module.exports = mongoose.model("Products", productSchema);